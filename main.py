from database import models
from database.connection import engine, SessionLocal
from sqlalchemy.orm import Session


from fastapi import FastAPI, HTTPException, Depends
from pydantic import BaseModel
from typing import Optional


app = FastAPI()

models.Base.metadata.create_all(bind=engine)


def get_db():
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


class Book(BaseModel):
    title: str
    author: str
    price: float


BOOKS = []


@app.get("/")
def read_root(db: Session = Depends(get_db)):
    return db.query(models.Books).all()


@app.get("/books/{book_id}")
def get_book_by_id(book_id: int, db: Session = Depends(get_db)):
    return db.query(models.Books).filter(models.Books.id == book_id).first()


@app.get("/books")
def get_book_by_title_or_author(*, title: Optional[str] = None, author: Optional[str] = None, db: Session = Depends(get_db)):
    book_title = db.query(models.Books).filter(
        models.Books.title == title).first()

    book_author = db.query(models.Books).filter(
        models.Books.author == author).first()

    if book_title:
        return book_title
    elif book_author:
        return book_author


@app.post("/books/{book_id}/create")
def add_book(book: Book, db: Session = Depends(get_db)):
    book_model = models.Books()
    book_model.title = book.title
    book_model.author = book.author
    book_model.price = book.price

    db.add(book_model)
    db.commit()
    return book


@app.put("/books/{book_id}/update")
def update_book(book_id: int, book: Book, db: Session = Depends(get_db)):
    book_model = db.query(models.Books).filter(
        models.Books.id == book_id).first()

    if book_model is None:
        raise HTTPException(
            status_code=404,
            detail=f"ID {book_id}: Doesn't exist"
        )

    book_model.title = book.title
    book_model.author = book.author
    book_model.price = book.price

    db.add(book_model)
    db.commit()

    return book


@app.delete("/books/{book_id}/delete")
def delete_book(book_id: int, db: Session = Depends(get_db)):
    book_model = db.query(models.Books).filter(
        models.Books.id == book_id).first()

    if book_model is None:
        raise HTTPException(
            status_code=404,
            detail=f"ID {book_id}: Doesn't exist"
        )

    db.query(models.Books).filter(
        models.Books.id == book_id).delete()

    db.commit()

    return "Book deleted successfully"
